<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Intervention\Image\Facades\Image;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dateOfBirth', 'account_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot(){
        parent::boot();
        static::creating(function($user){
            
           
        });
        static::created(function($user){
            
            $user->profile()->create();
            /*
            $account = Account::create([
                'name' => $user->name,
                'password' => $user->name,
            ]);*/
            //Account::where('name', '=', $user->name && 'password', '=', $user->name)->delete();
            //$user->account_id = $account->id;
            $user->save();
        });
        
    }
    

    public function calculaEdad() {
        $dia=date("d");
        $mes=date("m");
        $ano=date("Y");
        
        
        $dianaz=date("d",strtotime($this->dateOfBirth));
        $mesnaz=date("m",strtotime($this->dateOfBirth));
        $anonaz=date("Y",strtotime($this->dateOfBirth));
        
        
        //si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
        
        if (($mesnaz == $mes) && ($dianaz > $dia)) {
        $ano=($ano-1); }
        
        //si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
        
        if ($mesnaz > $mes) {
        $ano=($ano-1);}
        
         //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
        
        $edad=($ano-$anonaz);
        
        
        return $edad;

    }
  
    public function profile(){
        return $this->hasOne(Profile::class);
    }
 
   
    public function account(){
        $account = Account::findOrFail($this->account_id);
        return $account;
    }
}
