<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use App\User;
use App\Profile;

class ProfileController extends Controller
{
    protected function makeProfileImg(){
        $image = request('profileImg');
        $imagePath = $image->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000,1000);
        $image->save();
        
      //  $this->profileImg = $imagePath;
        //return $imagePath;
    }
   

    public function show(){
        $user = auth()->user();
       // $this->authorize('update', $user->profile);
        return view('profile');
    }

    public function store(){
        $user = auth()->user();
        //$this->authorize('update', $user->profile);
        $request = request() ->validate([
            'profileImg'=>['required','image'],
           
        ]);
        if(request('profileImg')){
            $image = request('profileImg');
            $imagePath = $image->store('uploads','public');
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000,1000);
            $image->save();
        }
        $user->profile->profileImg = $imagePath;
        $user->profile->save();
       
        return view('/home',[
            'user' => $user,
        ]);
 
     //dd( $user->account);
    }

    public function update (User $user){
        if(request('profileImg')){
            $image = request('profileImg');
            $imagePath = $image->store('uploads','public');
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000,1000);
            $image->save();
        }
        auth()->user()->profile->update([
            'profileImg' => $imagePath,
        ]);
    }

}
