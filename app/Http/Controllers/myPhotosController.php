<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use App\User;
use App\Photo;
use App\Account;

class myPhotosController extends Controller
{
    //protected $redirectTo = '/home';

    public function __construct(){
        //gracias a esta línea sólo podré acceder a los posts si estoy autenticado
        $this->middleware('auth');
    }
    public function index()
    {
        $user=auth()->user();
        return view('photos/myPhotos',[
            'user'=>$user,
        ]);
    }
    

    public function show(\App\Photo $photo){
        
      //dd($photo);
        return view('photos/show',compact('photo'));
    }

    public function showGame1(){
        $user=auth()->user();
        $extrangerPersons = ['Michelle Pfeifer', 'Aznar', 'Pepito Grillo','Hormiga atómica', 'Blancanieves','Harry Potter'];
        $KOs = [];
        $personsKO = [];
      do{
        $photo_id = array_rand($user->account()->photos->toArray());
        $photo = $user->account()->photos->get($photo_id);
        
        $personsOK = [];
        
        foreach($photo->tags as $tag){
            array_push($personsOK, $tag->name);
        }
        $cont = count($personsOK);
              
      }while($cont>4);

        if($cont != 4){
            $KOs = array_rand($extrangerPersons, (4 - $cont));
            $personsKO = [];
            if(is_array($KOs)){
                
                foreach($KOs as $ko){
                    array_push($personsKO, $extrangerPersons[$ko]);
                }
                
            }else{


                array_push($personsKO, $KOs);
            }
        }else{
            $personsKO = [];
        }
       
       

        $personsGame =array_merge($personsKO,$personsOK);
        shuffle($personsGame);
      
        return view('games/game1', compact(
            'photo',
            'personsGame'
        ));
   
        //dd($personsGame);
    }

    public function showResults(\App\Photo $photo){
        $answers = request('personsInPhoto');
        
        $user = auth()->user();
        $photoToCheck = $user->account()->photos->get($photo->id);
        $personsOK = [];
         
        foreach($photo->tags as $tag){
            array_push($personsOK, $tag->name);
        }
        $perfect = count($personsOK);
        $result = 0;
        if(is_array($answers)){
            foreach($answers as $answer){
                if(in_array($answer,$personsOK)){
                    $result++;
                }
            }
        }else{
            if(in_array($answers,$personsOK)){
                $result++;
            }
        }
        
        $resultPerCent = $result*100/$perfect;
        
        
        if($resultPerCent>60){
            $congrats = "Enhorabuena!";
        }else{
            $congrats = "Sigue practicando para poder mejorar tus resultados.";
        }
        $resultForUser = "Has acertado ".$result." de ".$perfect." personas. Eso supone un ".round($resultPerCent)."%.\n".$congrats;
        return view('games.results',[
            'resultForUser' => $resultForUser,
            'photo' => $photo,
        ]);
       // dd($resultForUser);

    }

    public function create($user){
        $user=User::findOrFail($user);
        return view('photos.create',[
            'user'=>$user,
        ]);
    }

    public function edit(\App\Photo $photo){
        
        return view('photos.edit',compact('photo'));
    }

    public function update(\App\Photo $photo){
        $data = request() ->validate([
            'description'=>'',
           // 'place'=>'',
           'tags'=>'',
        ]);
        
        $tags = explode(',', request('tags'));
        $photo->tag($tags);
        //$photoOld->description = $data['description'];
        $photo->update($data);
        $user=auth()->user();
        return view('photos.myPhotos',[
            'user'=>$user,
        ]);
    }

    public function store($user){
        
        $request = request() ->validate([
            'description'=>'',
           // 'place'=>'',
            'image'=>['required','image'],
            'tags'=>'',
        ]);
        
        $tags = explode(',', request('tags'));
        $imagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000,1000);
        $image->save();
        //gracias a esta línea me coge el id_user de la persona que está autenticada
        $photo = auth()->user()->account()->photos()->create([
            'description'=>$request['description'],
            'image'=>$imagePath,
        ]);
        $photo->tag($tags);

        $user=User::findOrFail($user);
        
        
        return view('photos.myPhotos',[
            'user'=>$user,
        ]);
        
    }
    public function delete(\App\Photo $photo){
        $delPhoto = Photo::findOrFail($photo)->delete();
        return view('photos.myPhotos');

    }
     
    

    
}
