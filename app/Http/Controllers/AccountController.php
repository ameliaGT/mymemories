<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;

class AccountController extends Controller
{
    
    public function update(){
        return view('account.registerAccount');
    }

    public function store(){
        $user = auth()->user();
        $data = request() ->validate([
            'name'=>'required', 
            'password' => 'required'
        ]);
      
        $account = Account::where('name', '=', $data['name'])->where('password', '=', $data['password'])->get();
                
        if($account->isEmpty()){
            $accountNew = Account::create([
                'name'=> $data['name'],
                'password' => $data['password'],
            ]);
            $user->account_id = $accountNew->id;
            $user->save();
        }  else{
            $user->account_id = $account->first()->id;
            $user->save();
        }
        return view('/home');
     
        //dd($account);
    }

   
    
     
}

