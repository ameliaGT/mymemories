<?php

namespace App;

use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Taggable;
    protected $fillable = [
        'description', 'image',
    ];
    public function account(){
        return $this->belongsTo(Account::class);
    }
    protected $guarded = [];
}
