<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'profileImg,'
    ];
    public function getProfileImage(){
        return ($this->profileImg) ? '/storage/'.$this->profileImg : '/img//uploadProfileImg.png';
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
