<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name', 'password',
    ];
   
    public function users(){
        return $this->hasMany(User::class);
    }

    public function photos(){
        return $this->hasMany(Photo::class);
    }
}
