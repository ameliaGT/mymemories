@extends('layouts.app')

@section('content')
<body>
<div class="container full-height">
    <div class="row borde">
        <div id="lateral" class="col-3 flex-column borde">
            <img src="{{Auth::user()->profile->getProfileImage()}}" style="width:70%;" class="col-12 rounded-circle">
            <div id="noticias" class="borde perfil" style="height: 60%">
                <div class="borde" style="text-align:left; border-bottom:1px solid #22290D">
                    Perfil: 
                </div>
                <div class="borde" style="">
                        {{ ucfirst (Auth::user()->name) }} 
                </div>
                <div class="borde" style="">
                {{Auth::user()->calculaEdad()}} años
                </div>
                <div class="borde" style="">
                        {{ date('d-m-Y') }} 
                </div>
                <div class="borde" style="">
                        {{ date('H:i') }} 
                </div>
            </div>
            <div id="upload" class="upload borde flex-center">
                <div class="borde title3" style="">
                    Volver
                </div>
                <div><a href="{{ url('/home') }}"> <img src="/img/atras.jpg" style="height:50px" class="" href=""></a></div>
            </div>
        </div>

        <div id="main" class="col-9">
            <div id="title2" class="borde title2" style="">
                    Cuantas personas conoces en la foto?
            </div>
            <div class="flex-center-column">
                <div class="borde centrado">
                    <img src="/storage/{{$photo->image}}" style="width:40%" class="borde">
                </div>
                <div class="borde flex-show">
                    <form action="{{ url('/games/resultGame1/'.$photo->id) }}" enctype="multipart/form-data" method="post">
                        @csrf
                    
                            <div id="" class="borde col-12" style="width:100%">
                            
                                @foreach($personsGame as $person)
                                
                                <div class="borde col-12 pt-2 pl-2">
                                  <label for="personsInPhoto[]" class=""> <input type="checkbox" class="" name="personsInPhoto[]" value="{{$person}}">{{$person}}</label></br>
                                </div>
                    
                                @endforeach
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="baseButton">
                                    {{ __('Comprobar!') }}
                                </button>
                            </div>
                    </form>
                </div>
            </div>
   


        </div>
    </div>
   
   
</div>
</body>
@endsection
