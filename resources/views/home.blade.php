@extends('layouts.app')

@section('content')
<body>
<div class="container full-height">
   
    <div class="row justify-content-center">
        <div id="botonera" class="col-sm-12 col-md-3 borde">
            @if(Auth::user()->account_id == 0)
            
            @else
        
            <div class="col-12 pt-8 borde">
                <a href="{{ url('/photos/myPhotos') }}" class="baseButton">myPhotos</a>
            </div>
            <div class="borde title3 accountFamily centrado">
               
                    
                    <img src="{{    url('/img/family.jpg')   }}" class="rounded-circle col-6 pt-4">
                    Cuenta familiar: {{ Auth::user()->account()->name }} 
                    @foreach(Auth::user()->account()->users as $user)
                    <div class="pt-4">
                        <img src="{{$user->profile->getProfileImage()}}" style="width:20%;" class="rounded-circle">
                        {{  $user->name }}  
                    </div>
                    @endforeach
               
            </div>  
            
            @endif
        </div>
        <div class="col-sm-12 col-md-6 flex-center-column borde" style="height:80%">
            <div class=" pt-5">
            @if(Auth::user()->account_id == 0)
                <div class="borde centrado accountFamily title3">
                Vinculate a una cuenta familiar ó crea una nueva para poder empezar!
                    <a href="{{   url('/account/update')   }}"> 
                        <img src="{{    url('/img/family.jpg')   }}" class="rounded-circle col-12 pt-4" style="">
                    </a>
                    
                </div>
            
            @else
            <img src="{{    '/img/brainTree2.jpg'   }}" style="height:200px" class="rounded-circle">
                
                
            @endif
                
             
            </div>
            <div class="title m-b-md centrado" style="">
                Hola              
                    {{ ucfirst (Auth::user()->name) }} 
                    !
                
            </div>
            <div class="borde title3 accountFamily centrado">

            @if(Auth::user()->account_id == 0)
                
            @else
                <div class="col-12 pt-4 borde" style="height:1fr">
                    <a href="{{ url('/games/game1') }}" class="baseButton">Jugar!</a>
                </div>
            @endif
            </div>
        </div>
        <div id="noticias" class="col-sm-12 col-md-3 borde perfil">
            <div class="pb-3 pr-4 pt-4">
            <a href="{{ url('/profile') }}">
                
                <img src="{{Auth::user()->profile->getProfileImage()}}" style="width:90%;" class="col-12 rounded-circle">
                </a>
            </div>
            <div class="borde" style="text-align:left; border-bottom:1px solid #22290D">
                   Perfil: 
            </div>
            <div class="borde" style="">
                    {{ ucfirst (Auth::user()->name) }} 
            </div>
            <div class="borde" style="">
                   {{Auth::user()->calculaEdad()}} años
            </div>
            <div class="borde" style="">
                    {{ date('d-m-Y') }} 
            </div>
            <div class="borde" style="">
                    {{ date('H:i') }} 
            </div>
            
        </div>
    </div>
    <div id="footer" class="col-12"></div>
   
</div>
</body>
@endsection
