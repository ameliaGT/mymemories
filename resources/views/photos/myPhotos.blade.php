@extends('layouts.app')

@section('content')
<body>
<div class="container full-height">
    <div class="row borde">
        <div id="lateral" class="col-3 flex-column borde">
            <div class="pl-4">
                <a href="{{ url('/profile') }}">
                    <img src="{{Auth::user()->profile->getProfileImage()}}" style="width:90%;" class="col-12 rounded-circle">
                </a>
            </div>
            <div id="noticias" class="borde perfil" style="height: 60%">
                <div class="borde" style="text-align:left; border-bottom:1px solid #22290D">
                    Perfil: 
                </div>
        
                <div class="borde" style="">
                        {{ ucfirst (Auth::user()->name) }} 
                </div>
                <div class="borde" style="">
                    {{Auth::user()->calculaEdad()}} años
                </div>
                <div class="borde" style="">
                        {{ date('d-m-Y') }} 
                </div>
                <div class="borde" style="">
                        {{ date('H:i') }} 
                </div>
            </div>
            <div id="upload" class="upload borde flex-center">
            <div class="borde title3" style="">
                    Volver
                </div>
                <div><a href="{{ url('/home') }}"> <img src="/img/atras.jpg" style="height:50px" class="" href=""></a></div>
            </div>
        </div>

        <div id="main" class="col-9">
            <div id="title2" class="borde title2" style="">
                    myPhotos
            </div>
            <div id="photosbox" class="borde" style="">
          
                @foreach($user->account()->photos as $photo)
                <div class="borde mediaElement"><a href="{{ url('/photos/'.$photo->id) }}"><img src="/storage/{{ $photo->image }}" style="" class="" href=""></div>

                @endforeach
                <div class="borde title3 mediaElement flex-center-column" style="">
                    <div class="centrado">
                    <a href="{{ url('/photos/create/'.$user->id) }}">
                        <img src="/img/upload.jpg" style="width: 70%; border:none" class="">
                    </a>
                    </div>
                    Upload
                
                </div>
                
            </div>
        </div>
    </div>
   
   
</div>
</body>
@endsection
