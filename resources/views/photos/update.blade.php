@extends('layouts.app')

@section('content')
<body>
<div class="container full-height">
    <div class="row borde">
        <div id="lateral" class="col-3 flex-column borde">
            <div class="pl-4">
                <img src="{{Auth::user()->profile->getProfileImage()}}" style="width:90%;" class="col-12 rounded-circle">
            </div>
            <div id="noticias" class="borde perfil" style="height: 60%">
                <div class="borde" style="text-align:left; border-bottom:1px solid #22290D">
                    Perfil: 
                </div>
                <div class="borde" style="">
                        {{ ucfirst (Auth::user()->name) }} 
                </div>
                <div class="borde" style="">
                    75 años
                </div>
                <div class="borde" style="">
                        {{ date('d-m-Y') }} 
                </div>
                <div class="borde" style="">
                        {{ date('H:i') }} 
                </div>
            </div>
            <div id="upload" class="upload borde flex-center">
                <div class="borde title3" style="">
                    Volver
                </div>
                <div><img src="/img/brainTree2.jpg" style="height:50px" class="" href=""></div>
            </div>
        </div>

        <div id="main" class="col-9">
            <div id="title2" class="borde title2" style="">
                    Editar photo
            </div>
            <div id="toUpload" class="borde" style="">
                <img src="/storage/{{ $photo->image }}" style="" class="borde" href="">
            </div>
            <form action="{{ url('/photos/update/'.$photo->id) }}" enctype="multipart/form-data" method="post">
                @csrf
                
                <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" autofocus>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                </div>
<!--   
                <div class="form-group row">
                            <label for="place" class="col-md-4 col-form-label text-md-right">{{ __('Lugar') }}</label>

                            <div class="col-md-6">
                                <input id="place" type="text" class="form-control @error('place') is-invalid @enderror" name="place" value="{{ old('place') }}" autofocus>

                                @error('place')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                </div>
-->
                <div class="form-group row">
                            <label for="person" class="col-md-4 col-form-label text-md-right">{{ __('Persona') }}</label>

                            <div class="col-md-6">
                                <input id="person" type="text" class="form-control @error('person') is-invalid @enderror" name="person" value="{{ old('person') }}" autofocus>

                                @error('person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                </div>
                <div class="form-group row">
                            <label for="person" class="col-md-4 col-form-label text-md-right">{{ __('Persona') }}</label>

                            <div class="col-md-6">
                                <input id="person" type="text" class="form-control @error('person') is-invalid @enderror" name="person" value="{{ old('person') }}" autofocus>

                                @error('person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                </div>
               
                <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="baseButton">
                                    {{ __('Editar foto!') }}
                                </button>
                            </div>
                        </div>

            </form>
        </div>
    </div>
   
   
</div>
</body>
@endsection
