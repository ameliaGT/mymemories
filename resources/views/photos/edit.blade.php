<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'myMemories') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  
    

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
  

    


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand d-flex " href="{{ url('/') }}">
                 <!--   {{ config('app.name', 'Laravel') }}-->
                 <div><img src="/img/brainTree2.jpg" style="height:30px; border-right:1px solid #333" class="pr-4 pt-2"></div>
                 <div class="pl-4 pt-2 navTags" style="font-size:1.8rem">myMemories</div>
                
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link navTags" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link navTags" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle navTags" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item navTags" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
<div class="tagsContainer container full-height">
    <div class="row borde">
        <div id="lateral" class="col-3 flex-column borde">
            <div class="pl-4">
                <img src="{{Auth::user()->profile->getProfileImage()}}" style="width:90%;" class="col-12 rounded-circle">
            </div>
            <div id="noticias tagsContainer" class="borde perfil" style="height: 60%">
                <div class="borde" style="text-align:left; border-bottom:1px solid #22290D">
                    Perfil: 
                </div>
                <div class="borde" style="">
                        {{ ucfirst (Auth::user()->name) }} 
                </div>
                <div class="borde" style="">
                        {{Auth::user()->calculaEdad()}} años        
                </div>
                <div class="borde" style="">
                        {{ date('d-m-Y') }} 
                </div>
                <div class="borde" style="">
                        {{ date('H:i') }} 
                </div>
            </div>
            <div id="upload" class="upload borde flex-center">
                <div class="borde titleTags3" style="">
                    Volver
                </div>
                <div><a href="{{ url('/photos/myPhotos') }}"> <img src="/img/atras.jpg" style="height:50px" class="" href=""></a></div>
            </div>
        </div>

        <div id="main" class="col-9">
            <div id="title2" class="borde titleTags2" style="">
                    Editar photo
            </div>
            <div id="toUpload" class="borde" style="">
                <img src="/storage/{{ $photo->image }}" style="width:50%" class="borde">
            </div>
            <form action="{{ url('/photos/update/'.$photo->id) }}" enctype="multipart/form-data" method="post"  class="formTags">
            
                @csrf
                @method('PATCH')
                    <div class="form-group row textTags">
                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>

                        <div class="col-md-6 textTags">
                            <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') ?? $photo->description }}" autofocus>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                </div>

                <div class="form-group row well">
                                            <label for="tags" class="col-md-4 col-form-label text-md-right">{{ __('Personas que aparecen (Palabras separadas por coma)') }}</label>
                                            <div class="col-md-6">
                                                <input id="tags" type="textarea" class="form-control" name="tags" data-role="tagsinput" style="">
                                            </div>
                                        
                                </div>

                                <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="baseButton">
                                                    {{ __('Actualizar!') }}
                                                </button>
                                            </div>
                                        </div>

                            </form>
                        </div>
                    </div>
                
   
</div>
</body>
</html>
