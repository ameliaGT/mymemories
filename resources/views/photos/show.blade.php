@extends('layouts.app')

@section('content')
<body>
<div class="container full-height">
    <div class="row borde">
        <div id="lateral" class="col-3 flex-column borde">
            <div class="pl-4">
                <img src="{{Auth::user()->profile->getProfileImage()}}" style="width:90%;" class="col-12 rounded-circle">
            </div>
            <div id="noticias" class="borde perfil" style="height: 60%">
                <div class="borde" style="text-align:left; border-bottom:1px solid #22290D">
                    Perfil: 
                </div>
                <div class="borde" style="">
                        {{ ucfirst (Auth::user()->name) }} 
                </div>
                <div class="borde" style="">
                {{Auth::user()->calculaEdad()}} años
                </div>
                <div class="borde" style="">
                        {{ date('d-m-Y') }} 
                </div>
                <div class="borde" style="">
                        {{ date('H:i') }} 
                </div>
            </div>
            <div id="upload" class="upload borde flex-center">
                <div class="borde title3" style="">
                    Volver
                </div>
                <div><a href="{{ url('/photos/myPhotos') }}"> <img src="/img/atras.jpg" style="height:50px" class="" href=""></a></div>
            </div>
        </div>

        <div id="main" class="col-9">
            <div id="title2" class="borde title2" style="">
                    myPhoto
            </div>
            <div class="flex-center-column">
                
                <img src="/storage/{{ $photo->image }}" style="width:80%" class="borde">
                
                <div id="description" class="borde importanText " style="">
                     {{$photo->description}}
                </div>
                <div id="persons" class="borde perfil" style="">
                    Personas que aparecen:
                    @foreach($photo->tags as $tag)
                    
                    <div class="label label-info centrado">{{ $tag->name }}</div>
           
                    @endforeach
                </div>

                <div class="col-3 pt-4 borde">
                    <a href="/photos/edit/{{ $photo->id }}" class="baseButton">Editar</a>
                </div>
            </div>


        </div>
    </div>
   
   
</div>
</body>
@endsection
