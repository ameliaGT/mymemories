<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Photo;
Route::get('/', function () {
    $user = auth()->user();
    return view('welcome',[
        'user'=>$user,
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//PHOTOS
Route::get('/photos/myPhotos', 'MyPhotosController@index');

Route::get('/photos/{photo}', 'MyPhotosController@show');

Route::get('/photos/create/{user}', 'MyPhotosController@create');

Route::get('/photos/delete/{photo}', 'MyPhotosController@delete');

Route::post('/photos/store/{account}', 'MyPhotosController@store');


Route::get('/photos/edit/{photo}', 'MyPhotosController@edit');

Route::patch('/photos/update/{photo}', 'MyPhotosController@update');

//ProfileImage
Route::get('profile', 'ProfileController@show');

Route::patch('profile', 'ProfileController@store');


//GAMES
Route::get('/games/game1','MyPhotosController@showGame1');
Route::post('/games/resultGame1/{photo}', 'MyPhotosController@showResults');



//ACCOUNTS
Route::get('/account/update', 'AccountController@update');
Route::post('/account/store', 'AccountController@store');
